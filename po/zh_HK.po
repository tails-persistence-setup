# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Tails developers
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# CasperLi_HK <casper.hk@hotmail.com>, 2013
# 大圈洋蔥, 2016
# CasperLi_HK <casper.hk@hotmail.com>, 2013
# ronnietse <tseronnie@ymail.com>, 2014
# ronnietse <tseronnie@ymail.com>, 2014
# 大圈洋蔥, 2016
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: Tails developers <tails@boum.org>\n"
"POT-Creation-Date: 2019-10-21 09:06+0200\n"
"PO-Revision-Date: 2016-06-06 08:15+0000\n"
"Last-Translator: carolyn <carolyn@anhalt.org>\n"
"Language-Team: Chinese (Hong Kong) (http://www.transifex.com/otf/torproject/"
"language/zh_HK/)\n"
"Language: zh_HK\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../lib/Tails/Persistence/Setup.pm:265
msgid "Setup Tails persistent volume"
msgstr "設定Tails持續磁碟區"

#: ../lib/Tails/Persistence/Setup.pm:343 ../lib/Tails/Persistence/Setup.pm:481
msgid "Error"
msgstr "錯誤"

#: ../lib/Tails/Persistence/Setup.pm:372
#, perl-format
msgid "Device %s already has a persistent volume."
msgstr "裝置%s已有持續磁碟區。"

#: ../lib/Tails/Persistence/Setup.pm:380
#, perl-format
msgid "Device %s has not enough unallocated space."
msgstr "裝置%s無足夠嘅未分配空間。"

#: ../lib/Tails/Persistence/Setup.pm:387 ../lib/Tails/Persistence/Setup.pm:401
#, perl-format
msgid "Device %s has no persistent volume."
msgstr "裝置%s無持續磁碟區。"

#: ../lib/Tails/Persistence/Setup.pm:393
#, fuzzy, perl-format
msgid ""
"Cannot delete the persistent volume on %s while in use. You should restart "
"Tails without persistence."
msgstr "無法刪除使用中嘅持續磁碟區。你須於唔使用持續磁碟區下重新啟動Tails。"

#: ../lib/Tails/Persistence/Setup.pm:407
#, fuzzy, perl-format
msgid "Persistence volume on %s is not unlocked."
msgstr "持續磁碟區未解鎖。"

#: ../lib/Tails/Persistence/Setup.pm:412
#, fuzzy, perl-format
msgid "Persistence volume on %s is not mounted."
msgstr "持續磁碟區未被掛接。"

#: ../lib/Tails/Persistence/Setup.pm:417
#, fuzzy, perl-format
msgid ""
"Persistence volume on %s is not readable. Permissions or ownership problems?"
msgstr "無法讀取持續磁碟區，權限定係擁有權問題？"

#: ../lib/Tails/Persistence/Setup.pm:422
#, fuzzy, perl-format
msgid "Persistence volume on %s is not writable."
msgstr "持續磁碟區未被掛接。"

#: ../lib/Tails/Persistence/Setup.pm:431
#, perl-format
msgid "Tails is running from non-USB / non-SDIO device %s."
msgstr "Tails正由非USB╱非SDIO裝置%s執行。"

#: ../lib/Tails/Persistence/Setup.pm:437
#, perl-format
msgid "Device %s is optical."
msgstr "裝置%s係可選嘅。"

#: ../lib/Tails/Persistence/Setup.pm:444
#, fuzzy, perl-format
msgid "Device %s was not created using a USB image or Tails Installer."
msgstr "裝置%s唔係用Tails安裝程式安裝。"

#: ../lib/Tails/Persistence/Setup.pm:688
msgid "Persistence wizard - Finished"
msgstr "持續嚮導 - 已完成"

#: ../lib/Tails/Persistence/Setup.pm:691
msgid ""
"Any changes you have made will only take effect after restarting Tails.\n"
"\n"
"You may now close this application."
msgstr ""
"你所作嘅任何更改都將於重新啟動Tails後生效。\n"
"\n"
"你咿㗎可以關閉此應用程式。"

#: ../lib/Tails/Persistence/Configuration/Setting.pm:113
msgid "Custom"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:55
msgid "Personal Data"
msgstr "個人資料"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:57
msgid "Keep files stored in the `Persistent' directory"
msgstr "檔案均儲存到Persistent目錄"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:70
#, fuzzy
msgid "Browser Bookmarks"
msgstr "瀏覽器書籤"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:72
msgid "Bookmarks saved in the Tor Browser"
msgstr "儲存喺Tor洋蔥路由瀏覽器嘅書籤"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:85
msgid "Network Connections"
msgstr "網絡連線"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:87
msgid "Configuration of network devices and connections"
msgstr "網絡裝置同連線設定"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:100
msgid "Additional Software"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:102
msgid "Software installed when starting Tails"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:120
msgid "Printers"
msgstr "打印機"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:122
msgid "Printers configuration"
msgstr "打印機設定"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:135
msgid "Thunderbird"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:137
msgid "Thunderbird emails, feeds, and settings"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:150
msgid "GnuPG"
msgstr "GnuPG"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:152
msgid "GnuPG keyrings and configuration"
msgstr "GnuPG匙圈同設定"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:165
#, fuzzy
msgid "Bitcoin Client"
msgstr "Bitcoin比特幣用戶端"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:167
msgid "Electrum's bitcoin wallet and configuration"
msgstr "Electrum嘅Bitcoin比特幣錢包同設定"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:180
msgid "Pidgin"
msgstr "Pidgin"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:182
msgid "Pidgin profiles and OTR keyring"
msgstr "Pidgin設定檔同唔記錄匙圈"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:195
msgid "SSH Client"
msgstr "SSH用戶端"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:197
msgid "SSH keys, configuration and known hosts"
msgstr "SSH鎖匙、設定同已知嘅主機"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:210
msgid "Dotfiles"
msgstr "Dotfiles"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:212
msgid ""
"Symlink into $HOME every file or directory found in the `dotfiles' directory"
msgstr "Symlink「dotfiles」目錄中搵到嘅每個檔案或目錄到$HOME"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:96
msgid "Persistence wizard - Persistent volume creation"
msgstr "持續嚮導 - 建立持續磁碟區"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:99
msgid "Choose a passphrase to protect the persistent volume"
msgstr "請選取一個通關句語嚟保護持續磁碟區"

#. TRANSLATORS: size, device vendor, device model
#: ../lib/Tails/Persistence/Step/Bootstrap.pm:103
#, perl-format
msgid ""
"A %s persistent volume will be created on the <b>%s %s</b> device. Data on "
"this volume will be stored in an encrypted form protected by a passphrase."
msgstr ""
"一個持續磁碟區%s將會被建立喺<b>%s %s</b>裝置上。此磁碟區上嘅資料將會加密咁儲"
"存，以通關句語保護。"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:108
msgid "Create"
msgstr "建立"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:151
#, fuzzy
msgid ""
"<b>Beware!</b> Using persistence has consequences that must be well "
"understood. Tails can't help you if you use it wrong! See the <i>Encrypted "
"persistence</i> page of the Tails documentation to learn more."
msgstr ""
"<b>小心！</b>使用持續性須了解其後果。如果錯用，Tails無法幫你。請參閱<a \"\n"
"\"href='file:///usr/share/doc/tails/website/doc/first_steps/persistence.en."
"html'>與持續性有關嘅Tails文件</a>了解更多。"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:179
msgid "Passphrase:"
msgstr "通關句語："

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:187
msgid "Verify Passphrase:"
msgstr "確認通關句語："

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:198
#: ../lib/Tails/Persistence/Step/Bootstrap.pm:266
msgid "Passphrase can't be empty"
msgstr "通關句語唔准空白"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:233
#, fuzzy
msgid "Show Passphrase"
msgstr "通關句語："

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:257
msgid "Passphrases do not match"
msgstr "通關句語唔相符"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:312
#: ../lib/Tails/Persistence/Step/Delete.pm:103
#: ../lib/Tails/Persistence/Step/Configure.pm:181
msgid "Failed"
msgstr "失敗"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:320
msgid "Mounting Tails persistence partition."
msgstr "掛接Tails持續分割區。"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:323
msgid "The Tails persistence partition will be mounted."
msgstr "Tails持續分割區將被掛接。"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:332
msgid "Correcting permissions of the persistent volume."
msgstr "修改持續磁碟區嘅權限。"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:335
msgid "The permissions of the persistent volume will be corrected."
msgstr "持續磁碟區嘅權限將被修改。"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:343
#, fuzzy
msgid "Creating default persistence configuration."
msgstr "正在儲存持續性設定…"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:346
#, fuzzy
msgid "The default persistence configuration will be created."
msgstr "Tails持續分割區將被掛接。"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:361
msgid "Creating..."
msgstr "建立中…"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:364
msgid "Creating the persistent volume..."
msgstr "正在建立持續磁碟區…"

#: ../lib/Tails/Persistence/Step/Delete.pm:53
msgid "Persistence wizard - Persistent volume deletion"
msgstr "持續嚮導 - 刪除持續磁碟區"

#: ../lib/Tails/Persistence/Step/Delete.pm:56
msgid "Your persistent data will be deleted."
msgstr "持續性資料將被刪除。"

#: ../lib/Tails/Persistence/Step/Delete.pm:60
#, perl-format
msgid ""
"The persistent volume %s (%s), on the <b>%s %s</b> device, will be deleted."
msgstr "持續磁碟區%s (%s)，喺<b>%s %s</b>裝置上，將被刪除。"

#: ../lib/Tails/Persistence/Step/Delete.pm:66
msgid "Delete"
msgstr "刪除"

#: ../lib/Tails/Persistence/Step/Delete.pm:117
msgid "Deleting..."
msgstr "刪除中…"

#: ../lib/Tails/Persistence/Step/Delete.pm:120
msgid "Deleting the persistent volume..."
msgstr "正在刪除持續磁碟區…"

#: ../lib/Tails/Persistence/Step/Configure.pm:88
msgid "Persistence wizard - Persistent volume configuration"
msgstr "持續嚮導 - 持續磁碟區設定"

#: ../lib/Tails/Persistence/Step/Configure.pm:91
msgid "Specify the files that will be saved in the persistent volume"
msgstr "指定將儲存喺持續磁碟區內嘅檔案"

#. TRANSLATORS: partition, size, device vendor, device model
#: ../lib/Tails/Persistence/Step/Configure.pm:95
#, perl-format
msgid ""
"The selected files will be stored in the encrypted partition %s (%s), on the "
"<b>%s %s</b> device."
msgstr "所選檔案將被儲存喺已加密嘅分割區%s (%s)，喺<b>%s %s</b>裝置上。"

#: ../lib/Tails/Persistence/Step/Configure.pm:101
msgid "Save"
msgstr "儲存"

#: ../lib/Tails/Persistence/Step/Configure.pm:195
msgid "Saving..."
msgstr "儲存中…"

#: ../lib/Tails/Persistence/Step/Configure.pm:198
msgid "Saving persistence configuration..."
msgstr "正在儲存持續性設定…"

#~ msgid "Icedove"
#~ msgstr "Icedove"

#~ msgid "Icedove profiles and locally stored email"
#~ msgstr "Icedove設定同本機所儲存嘅郵件"

#~ msgid "GNOME Keyring"
#~ msgstr "GNOME匙圈"

#~ msgid "Secrets stored by GNOME Keyring"
#~ msgstr "透過GNOME匙圈秘密儲存"

#~ msgid "APT Packages"
#~ msgstr "APT套件"

#~ msgid "Packages downloaded by APT"
#~ msgstr "透過APT下載嘅套件"

#~ msgid "APT Lists"
#~ msgstr "APT清單"

#~ msgid "Lists downloaded by APT"
#~ msgstr "透過APT下載嘅清單"

#~ msgid "Persistence volume is not writable. Maybe it was mounted read-only?"
#~ msgstr "持續磁碟區無法寫入。或許被掛接成唯讀？"
