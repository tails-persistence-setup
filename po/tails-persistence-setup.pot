# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Tails developers
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: Tails developers <tails@boum.org>\n"
"POT-Creation-Date: 2019-10-21 09:06+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../lib/Tails/Persistence/Setup.pm:265
msgid "Setup Tails persistent volume"
msgstr ""

#: ../lib/Tails/Persistence/Setup.pm:343 ../lib/Tails/Persistence/Setup.pm:481
msgid "Error"
msgstr ""

#: ../lib/Tails/Persistence/Setup.pm:372
#, perl-format
msgid "Device %s already has a persistent volume."
msgstr ""

#: ../lib/Tails/Persistence/Setup.pm:380
#, perl-format
msgid "Device %s has not enough unallocated space."
msgstr ""

#: ../lib/Tails/Persistence/Setup.pm:387 ../lib/Tails/Persistence/Setup.pm:401
#, perl-format
msgid "Device %s has no persistent volume."
msgstr ""

#: ../lib/Tails/Persistence/Setup.pm:393
#, perl-format
msgid ""
"Cannot delete the persistent volume on %s while in use. You should restart "
"Tails without persistence."
msgstr ""

#: ../lib/Tails/Persistence/Setup.pm:407
#, perl-format
msgid "Persistence volume on %s is not unlocked."
msgstr ""

#: ../lib/Tails/Persistence/Setup.pm:412
#, perl-format
msgid "Persistence volume on %s is not mounted."
msgstr ""

#: ../lib/Tails/Persistence/Setup.pm:417
#, perl-format
msgid ""
"Persistence volume on %s is not readable. Permissions or ownership problems?"
msgstr ""

#: ../lib/Tails/Persistence/Setup.pm:422
#, perl-format
msgid "Persistence volume on %s is not writable."
msgstr ""

#: ../lib/Tails/Persistence/Setup.pm:431
#, perl-format
msgid "Tails is running from non-USB / non-SDIO device %s."
msgstr ""

#: ../lib/Tails/Persistence/Setup.pm:437
#, perl-format
msgid "Device %s is optical."
msgstr ""

#: ../lib/Tails/Persistence/Setup.pm:444
#, perl-format
msgid "Device %s was not created using a USB image or Tails Installer."
msgstr ""

#: ../lib/Tails/Persistence/Setup.pm:688
msgid "Persistence wizard - Finished"
msgstr ""

#: ../lib/Tails/Persistence/Setup.pm:691
msgid ""
"Any changes you have made will only take effect after restarting Tails.\n"
"\n"
"You may now close this application."
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Setting.pm:113
msgid "Custom"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:55
msgid "Personal Data"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:57
msgid "Keep files stored in the `Persistent' directory"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:70
msgid "Browser Bookmarks"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:72
msgid "Bookmarks saved in the Tor Browser"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:85
msgid "Network Connections"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:87
msgid "Configuration of network devices and connections"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:100
msgid "Additional Software"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:102
msgid "Software installed when starting Tails"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:120
msgid "Printers"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:122
msgid "Printers configuration"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:135
msgid "Thunderbird"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:137
msgid "Thunderbird emails, feeds, and settings"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:150
msgid "GnuPG"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:152
msgid "GnuPG keyrings and configuration"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:165
msgid "Bitcoin Client"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:167
msgid "Electrum's bitcoin wallet and configuration"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:180
msgid "Pidgin"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:182
msgid "Pidgin profiles and OTR keyring"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:195
msgid "SSH Client"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:197
msgid "SSH keys, configuration and known hosts"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:210
msgid "Dotfiles"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:212
msgid ""
"Symlink into $HOME every file or directory found in the `dotfiles' directory"
msgstr ""

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:96
msgid "Persistence wizard - Persistent volume creation"
msgstr ""

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:99
msgid "Choose a passphrase to protect the persistent volume"
msgstr ""

#. TRANSLATORS: size, device vendor, device model
#: ../lib/Tails/Persistence/Step/Bootstrap.pm:103
#, perl-format
msgid ""
"A %s persistent volume will be created on the <b>%s %s</b> device. Data on "
"this volume will be stored in an encrypted form protected by a passphrase."
msgstr ""

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:108
msgid "Create"
msgstr ""

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:151
msgid ""
"<b>Beware!</b> Using persistence has consequences that must be well "
"understood. Tails can't help you if you use it wrong! See the <i>Encrypted "
"persistence</i> page of the Tails documentation to learn more."
msgstr ""

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:179
msgid "Passphrase:"
msgstr ""

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:187
msgid "Verify Passphrase:"
msgstr ""

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:198
#: ../lib/Tails/Persistence/Step/Bootstrap.pm:266
msgid "Passphrase can't be empty"
msgstr ""

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:233
msgid "Show Passphrase"
msgstr ""

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:257
msgid "Passphrases do not match"
msgstr ""

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:312
#: ../lib/Tails/Persistence/Step/Delete.pm:103
#: ../lib/Tails/Persistence/Step/Configure.pm:181
msgid "Failed"
msgstr ""

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:320
msgid "Mounting Tails persistence partition."
msgstr ""

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:323
msgid "The Tails persistence partition will be mounted."
msgstr ""

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:332
msgid "Correcting permissions of the persistent volume."
msgstr ""

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:335
msgid "The permissions of the persistent volume will be corrected."
msgstr ""

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:343
msgid "Creating default persistence configuration."
msgstr ""

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:346
msgid "The default persistence configuration will be created."
msgstr ""

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:361
msgid "Creating..."
msgstr ""

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:364
msgid "Creating the persistent volume..."
msgstr ""

#: ../lib/Tails/Persistence/Step/Delete.pm:53
msgid "Persistence wizard - Persistent volume deletion"
msgstr ""

#: ../lib/Tails/Persistence/Step/Delete.pm:56
msgid "Your persistent data will be deleted."
msgstr ""

#: ../lib/Tails/Persistence/Step/Delete.pm:60
#, perl-format
msgid ""
"The persistent volume %s (%s), on the <b>%s %s</b> device, will be deleted."
msgstr ""

#: ../lib/Tails/Persistence/Step/Delete.pm:66
msgid "Delete"
msgstr ""

#: ../lib/Tails/Persistence/Step/Delete.pm:117
msgid "Deleting..."
msgstr ""

#: ../lib/Tails/Persistence/Step/Delete.pm:120
msgid "Deleting the persistent volume..."
msgstr ""

#: ../lib/Tails/Persistence/Step/Configure.pm:88
msgid "Persistence wizard - Persistent volume configuration"
msgstr ""

#: ../lib/Tails/Persistence/Step/Configure.pm:91
msgid "Specify the files that will be saved in the persistent volume"
msgstr ""

#. TRANSLATORS: partition, size, device vendor, device model
#: ../lib/Tails/Persistence/Step/Configure.pm:95
#, perl-format
msgid ""
"The selected files will be stored in the encrypted partition %s (%s), on the "
"<b>%s %s</b> device."
msgstr ""

#: ../lib/Tails/Persistence/Step/Configure.pm:101
msgid "Save"
msgstr ""

#: ../lib/Tails/Persistence/Step/Configure.pm:195
msgid "Saving..."
msgstr ""

#: ../lib/Tails/Persistence/Step/Configure.pm:198
msgid "Saving persistence configuration..."
msgstr ""
